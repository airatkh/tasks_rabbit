#Обработчик, получает данные из queues в RabbitMq.#

##При получении пекета из rabbit запускает соответствующий обработчик
заданный в конфигурационном файле. ##

### Пример конфигурации###

```
#!api

                            'job_workers'=>[
                               'gnss' => [ //Keys - value 'cmd' in data from Rabbit
                               'class'=>'app\modules\obd\models\Gnss', // Class for processing data
                               'method'=>'saveOdbData', // method for processing data
                               ],
                               'ecu'=>[
                               'class'=>'app\modules\obd\models\Ecu',
                               'method'=>'saveOdbData',
                               ],
                               'alarm'=>[
                               'class'=>'app\modules\obd\models\Alarm',
                               'method'=>'saveOdbData',
                               ],
                               'sign'=>[
                              'class'=>'app\modules\obd\models\InternalEvent',
                              'method'=>'saveOdbData',
                               ],
```

### Основной файл, Фабрика созддания обработчика.###

### [TasksFactory](https://bitbucket.org/airatkh/tasks_rabbit/src/20dc39427b7686a6ea0e20373abe0fb23362b974/components/TasksFactory.php?at=master) ###