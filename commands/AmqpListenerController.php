<?php
/**
 * @link https://github.com/webtoucher/yii2-amqp
 * @copyright Copyright (c) 2014 webtoucher
 * @license https://github.com/webtoucher/yii2-amqp/blob/master/LICENSE.md
 */


namespace app\modules\tasks_rabbit\commands;

use Yii;
use PhpAmqpLib\Message\AMQPMessage;
use webtoucher\amqp\components\Amqp;
use webtoucher\amqp\components\AmpqInterpreterInterface;
use webtoucher\amqp\controllers\AmqpConsoleController;



/**
 * AMQP listener controller.
 *
 * @author Alexey Kuznetsov <mirakuru@webtoucher.ru>
 * @author Khisamov Airat <kh.airat14@gmail.com>
 * @since 2.0
 */
class AmqpListenerController extends AmqpConsoleController
{

    public $defaultAction = 'run';
    /**
     * Interpreter classes for AMQP messages. This class will be used if interpreter class not set for exchange.
     *
     * @var array
     */
    public $interpreter = [];

    protected $taskFactory;

    /**
     * Returns AMQP object.
     *
     * @return Amqp
     */
    public function getTasks_Rabbitmq()
    {
        if (empty($this->amqpContainer)) {
            $this->amqpContainer = Yii::$app->tasks_rabbitmq;
        }
        return $this->amqpContainer;
    }

    /**
     * Default action
     *
     * @param string $routingKey
     * @param bool   $queue
     * @param string $type
     */
    public function actionRun($routingKey = '#', $queue = false, $type = Amqp::TYPE_TOPIC)
    {
        \Yii::info('Task manager started. routingKey='.$routingKey.' queue='.$queue,__METHOD__);
        $this->tasks_rabbitmq->listen($routingKey, [$this, 'callback'],$queue ,$type);
    }

    /**
     * Функция сробатывает при получении данных из трубы RabbitMq
     *
     * @param AMQPMessage $msg
     *
     * @throws \Exception
     */
    public function callback(AMQPMessage $msg)
    {

        if (isset($this->interpreter['class']) || isset($this->interpreter['method'])){
            $class = $this->interpreter['class'];
            $method = $this->interpreter['method'];
        }else{
            $msg = 'Error configuration. Can not find param class or method. Ple check config file.';
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg, $code = 0);
        }

        $taskFactory = \Yii::$container->get($class, [$this->interpreter['job_workers'], $msg]);

        if (method_exists($taskFactory, $method)) {
            $taskFactory->$method($msg);
        }else{
            $msg = "Error: method = $method not defined in class = $class.";
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg, $code = 0);
        }
    }
}
