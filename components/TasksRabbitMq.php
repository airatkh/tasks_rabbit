<?php
/**
 * @link https://github.com/webtoucher/yii2-amqp
 * @copyright Copyright (c) 2014 webtoucher
 * @license https://github.com/webtoucher/yii2-amqp/blob/master/LICENSE.md
 */


namespace app\modules\tasks_rabbit\components;


use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPConnection;
use webtoucher\amqp\components\Amqp;

/**
 * TasksRabbitMq wrapper.
 *
 * @property AMQPConnection $connection AMQP connection.
 * @property AMQPChannel $channel AMQP channel.
 * @author Alexey Kuznetsov <mirakuru@webtoucher.ru>
 * @author Khisamov Airat <kh.airat14gmail.com>
 * @since 2.0
 */
class TasksRabbitMq extends Amqp
{
    /**
     * Listens the exchange for messages.
     *
     * @param string      $routing_key
     * @param callable    $callback
     * @param bool        $queue
     * @param bool|string $type
     *
     * @internal param string $exchange
     */
    public function listen($routing_key, $callback, $queue = false, $type = self::TYPE_TOPIC)
    {
        $this->channel->queue_declare($queue, false, $durable = true, false,false);
        $this->channel->basic_qos(null,1,null);
        $this->channel->basic_consume($queue, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->channel->close();
        $this->connection->close();
    }

    /**
     * Sends message to the exchange.
     *
     * @param string $exchange
     * @param string $routing_key
     * @param string|array $message
     * @param string $type Use self::TYPE_DIRECT if it is an answer
     * @return void
     */
    public function send($exchange, $routing_key, $message, $type = self::TYPE_TOPIC)
    {
        $message = $this->prepareMessage($message);
        if ($type == self::TYPE_TOPIC) {
            $this->channel->queue_declare($routing_key, false, true, false, false);
        }
        $this->channel->basic_publish($message, '', $routing_key);
    }
}
