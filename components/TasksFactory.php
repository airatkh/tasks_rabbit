<?php

namespace app\modules\tasks_rabbit\components;


use Yii;
use PhpAmqpLib\Message\AMQPMessage;
use webtoucher\amqp\components\AmqpInterpreter;
use app\modules\obd\models;
use yii\base\Exception;
use yii\di\ServiceLocator;
use yii\helpers\Json;
use yii\helpers\VarDumper;

class TasksFactory extends AmqpInterpreter
{

    /**
     * Interpreter classes for AMQP messages. This class will be used if interpreter class not set for exchange.
     *
     * @var array
     */
    protected $job_workers = [];

    /**
     * AMQPMessage object. Package from RabbitMq.
     */
    private $msg;

    /**
     * @param $data array Data from rabbit
     */
    protected $data = [];

    /**
     * @param $job_workers array массив ассоциации task => worker
     *                           <code>
     *                            'job_workers'=>[
     *                               'gnss' => [ //Keys - value 'cmd' in data from Rabbit
     *                               'class'=>'app\modules\obd\models\Gnss', // Class for processing data
     *                               'method'=>'saveOdbData', // method for processing data
     *                               ],
     *                               'ecu'=>[
     *                               'class'=>'app\modules\obd\models\Ecu',
     *                               'method'=>'saveOdbData',
     *                               ],
     *                               'alarm'=>[
     *                               'class'=>'app\modules\obd\models\Alarm',
     *                               'method'=>'saveOdbData',
     *                               ],
     *                               'sign'=>[
     *                               'class'=>'app\modules\obd\models\InternalEvent',
     *                               'method'=>'saveOdbData',
     *                               ],
     *                           </code>
     *
     * @param $msg AMQPMessage object. Package from RabbitMq.
     *
     * @throws \yii\base\Exception
     */
    public function __construct($job_workers, $msg)
    {
        if(!is_array($job_workers)){
            \Yii::error($msg,__METHOD__);
            throw new Exception("Param job_workers have to be array. recieved=".VarDumper::export($job_workers));
        }

        if(empty($job_workers)){
            \Yii::error($msg,__METHOD__);
            throw new Exception("Param job_workers have not to be empty. recieved=".VarDumper::export($job_workers));
        }

        if (!$msg instanceof AMQPMessage) {
            \Yii::error($msg,__METHOD__);
            throw new Exception("Class is not correct for msg param. recieved=".VarDumper::export($msg));
        }

        $this->job_workers = $job_workers;
        $this->msg = $msg;
        $this->data = Json::decode($this->msg->body, true);
    }

    /**
     * Check configuration.
     * Check that all necessary parameters exist
     *
     * 1. Убедиться что есть ключи в массиве
     * 1.1 data['cmd']
     * 1.2 ключ соответствующий значению data['cmd'] в массиве  $this->job_workers
     * 1.3 ключ 'class' есть в массиве  data['cmd']
     * 1.4 ключ 'method' есть в массиве  data['cmd']
     * 2. Проверить что есть ссответствующий class и method заданные в конфигурации
     *
     * @throws \Exception
     * @internal param array $data data about task from RabbitMq
     *
     * @return array  names class and method for worker
     */
    protected function getConfigInstance(){

        $data = $this->data;

        if(!isset($data['cmd'])){
            $msg = "Error: key 'cmd' not defined in data array. Is is required parameter. data=".VarDumper::export($data);
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        if( !isset($this->job_workers[ $data['cmd'] ]) ){
            $msg = sprintf("Error: key %s not defined in array %s.", $data['cmd'], VarDumper::export($this->job_workers));
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        $worker_config = $this->job_workers[$data['cmd']];

        if( !isset($worker_config['class']) ){
            $msg = sprintf("Error: required key 'class' not defined in array %s.", VarDumper::export($worker_config));
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        if( !isset($worker_config['method']) ){
            $msg = sprintf("Error: required key 'method' not defined in array %s.", VarDumper::export($worker_config));
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        $worker = $worker_config['class'];
        $method = $worker_config['method'];

        self::checkWorkerMethodExist($worker, $method);
        return [$worker, $method];

    }

    /**
     * Check exist class and method in that class
     *
     * @param $worker string Class worker name
     * @param $method string Method of this class
     *
     * @return bool true if exist
     * @throws \Exception if not exit throw Exception
     */
    static protected function checkWorkerMethodExist($worker, $method){

        if(class_exists($worker)){
            if (method_exists($worker, $method)) {
                return true;
            }else{
                $msg = "Error: method = $method not defined in class = $worker.";
                \Yii::error($msg,__METHOD__);
                throw new \Exception($msg);
            }
        }else{
            $msg = "Error: class = $worker not defined.";
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * Depending of result we delete package from rabbit or
     * throw Exception with information to log file
     *
     * @param $result bool result
     *
     * @throws \Exception
     */
    protected function setTaskResult($result){
        if($result){
            //Ask RabbitMq that data received, that it can be deleted from rabbitMq
            \Yii::info( 'Saved: '.VarDumper::export($this->data), __METHOD__);
            $this->msg->delivery_info['channel']->basic_ack($this->msg->delivery_info['delivery_tag']);
        }else{
            //This package will remain in the pipe rabbit. We have not deleted it.
            $message = 'Can not save data: '.VarDumper::export($this->data);
            \Yii::error($message,__METHOD__);
            throw new \Exception($message, $code = 0);
        }
    }

    /**
     * Main method for creation worker by existing task
     *
     * 1. Извлеч проверенные данные из конфигурации обработчика задачи.
     * 2. Запустить данный обработчик
     * 3. Установить возвращаемое значение.
     */
    public function tasksFactory()
    {
        list($worker, $method) = $this->getConfigInstance();
        $worker_name = $worker; $worker_method = $method;
        \Yii::info('Worker will start: worker='.$worker_name.' method='.$worker_method,__METHOD__);

        $worker = \Yii::$container->get($worker, $params=[$this->data['cmd'], Yii::$app]);
        $result = $worker->$method($this->data);
        \Yii::info('Worker finished : worker='.$worker_name.' method='.$worker_name.' result='.$result,__METHOD__);
        $this->setTaskResult($result);

    }
}
